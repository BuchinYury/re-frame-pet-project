(ns client.views
  (:require [re-frame.core :as rf]
            [garden.core :as garden]
            [client.events]
            [client.subs]))

; title
(def title-style
  [:.title {:text-align    "center"
            :margin-bottom "10px"}])

(defn title
  []
  [:h1.title
   [:style (garden/css title-style)]
   @(rf/subscribe [:title])])

; navbar
(def navbar-style
  [[:.navbar {:display          "flex"
              :background-color "#EDEDED"}]
   [:.navbar-element {:padding     "10px"
                      :margin      "10px"
                      :border      "1px solid steelblue"
                      :user-select "none"}]
   [:.navbar-element.click {:background-color "steelblue"
                            :color            "white"
                            :user-select      "none"}]])

(defn navbar-item
  [{:keys [key item-text class]}]
  (let [on-click #(rf/dispatch [:current-screen item-text])]
    [:div {:key      key
           :class    class
           :on-click on-click}
     item-text]))

(defn navbar
  []
  (let [items @(rf/subscribe [:navbar-elements])]
    (into [:div.navbar
           [:style (garden/css navbar-style)]]
          (for [item items]
            (let [item-text      (:screen item)
                  current-screen @(rf/subscribe [:current-screen])
                  class          (if (= item-text current-screen) "navbar-element click" "navbar-element")
                  key            (gensym)]
              (navbar-item {:key key :item-text item-text :class class}))))))

; default-body
(defn default-body
  []
  [:div#default-body [:h4 "Comming soon"]])

; to-do
(def to-do-style
  [[:#todo-app {:background-color "#E3C4C4"
                :padding          "5px"
                :width            "600px"
                :margin           "0 auto"}]])

(defn to-do-body
  []
  (let [items     (rf/subscribe [:to-do-items])
        new-item  (rf/subscribe [:new-item])
        progress  (rf/subscribe [:progress])
        on-change #(rf/dispatch [:on-change [:to-do :new-item] (.. % -target -value)])
        on-submit #(do
                     (rf/dispatch [:add-item @new-item])
                     (rf/dispatch [:on-change [:to-do :new-item] ""]))]
    [:div#todo-app
     [:style (garden/css to-do-style)]

     (for [i @items]
       [:div.item {:key (or (:id i) (:title i))}
        (:title i)])
     [:br]
     (when-let [p @progress]
       [:div.item "Loading..."])
     [:input {:on-change on-change
              :value     @new-item}]
     [:button.btn.btn-success {:on-click on-submit} "Add"]]))

; yandex-geocoder
(def yandex-geocoder-style
  [[:input.geo-input {:font-size   "24px"
                      :width       "20em"
                      :line-height "1.5em"}]
   [:p.input-label {:font-size    "24px"
                    :display      "inline-block"
                    :width        "max-content"
                    :margin-right "1em"}]
   [:p.address {:margin-left "5em"}]
   [:div.input {:margin  "0 auto"
                :display "table"}]])

(defn yandex-geocoder-body
  []
  [:div
   [:style (garden/css yandex-geocoder-style)]
   [:br]
   [:div.input
    [:p.input-label "Input address:"]
    [:input.geo-input {:type      "text"
                       :value     @(rf/subscribe [:search-addresses-string])
                       :on-change #(rf/dispatch [:search-addresses-string-change (-> % .-target .-value)])}]]
   (when-let [addresses @(rf/subscribe [:addresses])]
     (for [address addresses]
       (let [address (get-in address [:GeoObject :metaDataProperty :GeocoderMetaData :Address :formatted])]
         [:p.address address])))
   [:br]
   [:br]
   (when-let [bad @(rf/subscribe [:bad-api-result])]
     (str ":bad-api-result - " bad))])

; shop
(def shop-style
  [[:.shop {:width  "1200px"
            :margin "auto"}]
   [:.shop-items {:display         "flex"
                  :flex-direction  "row"
                  :flex-wrap       "wrap"
                  :justify-content "center"
                  :align-content   "center"}]
   [:.shop-item {:display        "flex"
                 :flex-direction "column"
                 :flex-wrap      "wrap"
                 :border         "1px solid #000000"
                 :margin         "20px"
                 :padding        "20px"}]
   [:.shop-button {:margin-top "20px"
                   :height     "30px"
                   :background "steelblue"
                   :color      "white"
                   :outline    "none"}]
   [:.shop-button:active {:background "#5A6572"
                          :color      "white"}]])

(defn shop-item-card
  [{:keys [key img-src]}]
  (let [on-click #()]
    [:div.shop-item {:key key}
     [:img {:src   img-src
            :style {:border "1px solid #00000030"}}]
     [:button.shop-button "Добавить"]]))

(defn shop-body
  []
  (let [items @(rf/subscribe [:shop-items])]
    [:div.shop
     [:style (garden/css shop-style)]
     [:p {:style {:margin-left "20px"}}
      "Shop Screen"]
     (into [:div.shop-items]
           (for [item items]
             (let [key       (gensym)
                   item-text (:src item)]
               (shop-item-card {:key key :img-src item-text}))))]))

; root
(def core-style
  [[:div :h1 {:font-family "HelveticaNeue, Helvetica"
              :color       "#777"
              :margin      "0"}]
   [:.app {:background-color "white"
           :padding          "5px"}]])

(defn root-view
  []
  [:div.app
   [:style (garden/css core-style)]
   [title]
   [navbar]
   (when-let [current-screen @(rf/subscribe [:current-screen])]
     [:div#app-body
      (cond
        (= "To-Do" current-screen) [to-do-body]
        (= "Yandex Geocoder" current-screen) [yandex-geocoder-body]
        (= "Shop" current-screen) [shop-body]
        :else [default-body])])
   [:p (str @(rf/subscribe [:db-state]))]])
