(ns client.routing
  (:require-macros [cljs.core.async.macros :refer (go)])

  (:require [reagent.core :as reagent :refer (atom)]
            [cljs.core.async :refer (chan put! <!)]
            [secretary.core :as secretary :refer-macros [defroute]]
            [goog.events :as events]
            [goog.history.EventType :as EventType])

  (:import goog.History))

(secretary/set-config! :prefix "#")

(defroute "/users/:id" {:as params}
          (js/console.log (str "User: " (:id params))))

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen EventType/NAVIGATE #(secretary/dispatch! (.-token %)))
    (.setEnabled true)))

(hook-browser-navigation!)

;(def EVENTCHANNEL (chan))
;
;(def app-state
;    (reagent/atom
;        {   :text "Hello"
;            :items [{:display "Item 1"}
;                    {:display "Item 26"}
;                    {:display "Item 3"}
;                    {:display "Item 46"}
;                    {:display "Item 5"}
;                    ]
;            :active-item {}
;        }
;    )
;)
;
;(def EVENTS
;    {:update-active-item    (fn [{:keys [active-item]}]
;                                (swap! app-state assoc-in [:active-item] active-item)
;                            )
;    }
;)
;
;(go
;    (while true
;        (let [[event-name event-data] (<! EVENTCHANNEL)]
;            (js/console.log (str event-name " !-! " event-data))
;            ((event-name EVENTS) event-data)
;        )
;    )
;)
;
;
;#_(defn say-hello []
;    (js/console.log "Hello, World!")
;    (js/console.log js/React)
;    )
;
;#_(say-hello)
;
;
;
;
;
;#_(def current-count (reagent/atom 0))
;
;#_(defn counter []
;    [:p {:on-click (fn [event]
;                        (swap! current-count (fn [old-state] (inc old-state))) @current-count
;                    )
;        }
;    ]
;)
;
;
;(defn app []
;    #_[:div
;        [:h2 {:id "h2" :class "title"} "Hello from a TagFigts"]
;
;        [:div {:id "ko"}
;            (for [i (range 10)]
;                [:h3 {:id i} i]
;                )
;            ]
;
;        [:h1 {:class (if (> 5 3) "active")} (:text @app-state) " Some"]
;
;        [:div {:class "container"}
;            [title/header (:text @app-state)]]
;    ]
;
;    [:div {:class "container"}
;        [title/header (:text @app-state)]
;        [title/items-list EVENTCHANNEL (:items @app-state) (:active-item @app-state)]
;    ]
;)
;
;#_(defn change-text-state []
;    (while true
;    (if (= (/ js/Date 10) 5)
;        (swap! app-state assoc :text-state 10)
;        (swap! app-state assoc :text-state 2)
;        )
;    )
;)
;
;
;(reagent/render [app] (js/document.querySelector "#cljs-target"))
;
;(js/setTimeout
;    (fn []
;        (swap! app-state assoc-in [:text] "New Message>>>"))
;    2000)
;
;#_(change-text-state)
;
;
;(comment
;    (require '[fights.main :as fm])
;    (swap! fm/app-state assoc-in [:text] "New Hello")
;)