(ns client.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
  :db-state
  (fn [db _] db))

; title
(rf/reg-sub
  :title
  (fn [db _]
    (:title db)))

; current-screen
(rf/reg-sub
  :current-screen
  (fn [db _]
    (:current-screen db)))

; navbar
(rf/reg-sub
  :navbar-elements
  (fn [db _]
    (:navbar-elements db)))

; to-do
(defn items-sub [db _]
  (get-in db [:to-do :items]))

(rf/reg-sub :to-do-items items-sub)

(defn to-do [db _]
  (get db :to-do))

(rf/reg-sub :to-do to-do)

(rf/reg-sub
  :new-item
  (fn [db _]
    (get-in db [:to-do :new-item])))

(rf/reg-sub
  :progress
  (fn [db _]
    (get-in db [:to-do :progress])))

; yandex-geocoder
(rf/reg-sub
  :search-addresses-string
  (fn [db _]
    (get-in db [:yandex-geocoder :search-addresses-string])))

(rf/reg-sub
  :addresses
  (fn [db _]
    (get-in db [:yandex-geocoder :addresses])))

(rf/reg-sub
  :bad-api-result
  (fn [db _]
    (get-in db [:yandex-geocoder :bad-api-result])))

; shop
(rf/reg-sub
  :shop-items
  (fn [db _]
    (get-in db [:shop :items])))
