(ns client.db)

(def initial_db
  {:title           "Автоальянс"
   :navbar-elements [;{:screen "To-Do"}
                     ;{:screen "Yandex Geocoder"}
                     ;{:screen "Shop"}
                     ;{:screen "Avto Aliance"}
                     {:screen "Магазины"}
                     {:screen "Бренды"}
                     {:screen "О компании"}]
   :stores          {}

   :to-do           {:items [{:title "item 1"}
                             {:title "item 2"}
                             {:title "item 4"}]}
   :yandex-geocoder {:search-addresses-string ""}
   :shop            {:items  [{:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}
                              {:src "https://lh3.googleusercontent.com/fhDauQL0p-8Fo1xiox7PBscp9ESjMxIRASPoNcGTCc59nXPkDb7LT-GAeo9sUE5OvY8=s180"}]
                     :basket ""}})