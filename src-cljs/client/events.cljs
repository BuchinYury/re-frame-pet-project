(ns client.events
  (:require [re-frame.core :as rf]
            [client.db :as db]
            [ajax.core :as ajax]
            [day8.re-frame.http-fx]))
; init db
(rf/reg-event-db
  :initialize
  (fn [_ _] db/initial_db))

; title
(defn body
  [db [_ current-screen]]
  (-> (assoc db :title current-screen)
      (assoc :current-screen current-screen)))

(rf/reg-event-db :current-screen body)

; navbar
(rf/reg-event-db
  :navbar-element-click
  (fn [db [_ id]]
    (let [navbar_elements (:navbar-elements db)
          navbar_click_class "navbar-element click"
          navbar_init_class "navbar-element"]
      (as-> (map #(if (== (:id %) id)
                    (assoc % :class navbar_click_class)
                    (assoc % :class navbar_init_class))
                 navbar_elements) v
            (vec v)
            (assoc db :navbar-elements v)))))

; to-do
(defn add-item [{db :db} [_ txt]]
  (let [item {:id    (str (gensym))
              :title txt}]
    {:db  (assoc-in db [:to-do :progress] "Loading")
     :xhr {:uri     "https://myserver"
           :body    item
           :success [:item-saved]}}))

(rf/reg-event-db :item-saved
                 (fn [db [_ item]]
                   (-> db
                       (update-in [:to-do :items] conj item)
                       (update :to-do dissoc :progress))))

(defn to-json [x]
  (.stringify js/JSON (clj->js x)))

(rf/reg-fx
  :xhr
  (fn [{body :body succ :success :as opts}]
    (js/setTimeout
      #(rf/dispatch (conj succ body))
      1000)))

(rf/reg-event-fx :add-item add-item)

(rf/reg-event-db
  :on-change
  (fn [db [_ pth val]]
    (assoc-in db pth val)))

; yandex geocoder
(rf/reg-event-fx
  :search-addresses-string-change
  (fn [{:keys [db]} [_ val]]
    {:db         (assoc-in db [:yandex-geocoder :search-addresses-string] val)
     :http-xhrio {:method          :get
                  :uri             (str "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=" val)
                  :timeout         8000
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:good-http-result]
                  :on-failure      [:bad-http-result]}}))

(rf/reg-event-db
  :good-http-result
  (fn [db [_ {{{:keys [featureMember]} :GeoObjectCollection} :response}]]
    (assoc-in db [:yandex-geocoder :addresses] featureMember)))

(rf/reg-event-db
  :bad-http-result
  (fn [db [_ result]]
    (assoc-in db [:yandex-geocoder :bad-api-result] result)))
