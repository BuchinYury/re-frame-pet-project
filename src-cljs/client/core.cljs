(ns client.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as rf]
            [client.views :as views]))

(rf/dispatch-sync [:initialize])
(reagent/render [views/root-view] (js/document.getElementById "app"))
