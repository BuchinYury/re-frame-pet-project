(ns server.ws.receiver
  (:require [cheshire.core :as cheshire]
            [org.httpkit.server :as ohs])
  (:import (com.fasterxml.jackson.core JsonParseException)))

(def clients (atom {}))

(defn chat-receiver
  "Chat receiver"
  [data]
  (doseq [client (keys @clients)]
    (ohs/send! client (cheshire/generate-string {:key "chat" :data data}))))

(defn waiter-receiver
  [ch]
  (fn [data]
    (let [ws-request (try (cheshire/parse-string data true)
                          (catch JsonParseException e data))
          method (get ws-request :method "unknown")
          id (get-in ws-request [:args :id] "unknown")]
      (println data)
      (cond
        (= method "waiter/register") (swap! clients assoc ch id)
        (= method "waiter/unregister") (swap! clients dissoc ch)
        :else (ohs/send! ch data)))))