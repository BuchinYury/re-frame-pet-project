(ns server.ws.handler
  (:require [org.httpkit.server :as ohs]
            [server.ws.receiver :as receiver]
            [server.models :as models]
            [cheshire.core :as cheshire]))

;; Главный обработчик (handler)
(defn ws-handler
  "Main WebSocket handler"
  [request]                                                 ;; Принимает запрос
  (ohs/with-channel request ch                              ;; Получает канал
                    (swap! receiver/clients assoc ch "unknown") ;; Сохраняем пул клиентов с которыми установлено соединение в атом clients и ставим флаг true
                    (println ch "Connection established")
                    (ohs/on-close ch (fn [status]
                                       (println "channel closed: " status)
                                       (swap! receiver/clients dissoc ch true))) ;; Устанавливает обработчик при закрытии канала
                    (ohs/on-receive ch (server.ws.receiver/waiter-receiver ch)
                                    #_(get receiver/receivers :chat))))

(defn show-msg
  [request]
  (doseq [client (keys @receiver/clients)]
    (ohs/send! client (cheshire/generate-string models/show-order)))
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    "Notification send"})

(defn hide-msg
  [request]
  (doseq [client (keys @receiver/clients)]
    (ohs/send! client (cheshire/generate-string models/hide-order)))
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    "Notification hide"})

(defn canceled-msg
  [request]
  (doseq [client (keys @receiver/clients)]
    (ohs/send! client (cheshire/generate-string models/canceled-order)))
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    "Notification canceled"})