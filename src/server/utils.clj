(ns server.utils
  (:import (java.security MessageDigest)))

(defn parseInt [int?]
  (try
    (Integer/parseInt int?)
    (catch Exception e int?)))

(defn md5 [token]
  (let [hash-bytes (doto (MessageDigest/getInstance "MD5")
                       (.reset)
                       (.update (.getBytes token)))]
    (.toString (new BigInteger 1 (.digest hash-bytes)) 16)))