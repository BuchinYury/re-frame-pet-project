(ns server.core
  (:require [org.httpkit.server :as ohs]
            [compojure.core :as compojure]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [clojure.java.io :as io]
            [cheshire.core :as cheshire]

            [server.models :as models]
            [server.utils :as utils]
            [server.ws.handler :as wsh]
            [server.ws.receiver :as wsr])
  (:gen-class))

(defonce server (atom nil))

(def index_html (slurp (io/resource "public/index.html")))

(compojure/defroutes app-routes
                     (compojure/GET "/" [] index_html)
                     (compojure/context "/waiter" []
                       (compojure/GET "/:login" [login]
                         (let [login (utils/parseInt login)]
                           (cheshire/generate-string (models/waiter login) {:pretty true})))
                       (compojure/GET "/:l/orders" [status]
                         (cheshire/generate-string
                           (models/orders status) {:pretty true})))
                     (compojure/context "/ws" []
                       (compojure/GET "/" [] wsh/ws-handler)
                       (compojure/GET "/clients" [] (cheshire/generate-string
                                                      @wsr/clients {:pretty true}))
                       (compojure/GET "/show-msg" [] wsh/show-msg)
                       (compojure/GET "/hide-msg" [] wsh/hide-msg)
                       (compojure/GET "/canceled-msg" [] wsh/canceled-msg))
                     (route/resources "/")
                     (route/files "/static/")
                     (route/not-found "<h3>Страница не найдена</h3>"))

(def app
  (handler/site #'app-routes))

(defn -main [& args]
  (println "Starting web server on port 3030")
  (reset! server (ohs/run-server #'app {:port 3030})))

(defn stop-server []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))

(comment
  (-main)
  (stop-server)

  (def server (-main))
  (server))
