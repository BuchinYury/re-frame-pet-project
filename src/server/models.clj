(ns server.models
  (:require [server.utils :as utils])
  (:import (java.util Date)))

(defn waiter [login]
  {:data   {:waiter {:id            12
                     :name          "Иванов Петр Сергеевич"
                     :login         login
                     :password_hash (utils/md5 "p")}}
   :errors nil})

(defn order [status]
  {:id         (rand-int 1000)
   :name       "10/11"
   :number     12
   :table      {:id   2
                :name "3"}
   :created_at (Date.)
   :status     status})

(def ordersStatusNew {:data   {:orders [(order "new")]}
                      :errors nil})

(def ordersStatusStarted {:data   {:orders [(order "started")
                                            (order "started")]}
                          :errors nil})

(def ordersStatusFinished {:data   {:orders [(order "finished")
                                             (order "finished")
                                             (order "finished")]}
                           :errors nil})

(defn orders [status]
  (cond
    (= status "new") ordersStatusNew
    (= status "started") ordersStatusStarted
    (= status "finished") ordersStatusFinished
    :else "unknown"))

(def notification_order
  {:id         10
   :name       "10/11"
   :number     12
   :table      {:id   2
                :name "3"}
   :created_at (Date.)
   :status     "finished"})

(def show-order {:method "events/order-finished"
                 :response notification_order})

(def hide-order {:method "events/order-completed"
                 :response notification_order})

(def canceled-order {:method "events/order-canceled"
                     :response notification_order})




