(defproject re-frame-pet "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.238"]
                 [org.clojure/core.async "0.4.474"]

                 [http-kit "2.3.0"]
                 [compojure "1.6.1"]
                 [cheshire "5.8.1"]
                 [javax.servlet/servlet-api "2.5"]

                 [garden "1.3.5"]
                 [reagent "0.7.0"]
                 [re-frame "0.10.5"]
                 [day8.re-frame/http-fx "0.1.6"]
                 [secretary "1.2.3"]]

  :main server.core

  :plugins [[lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
            [lein-ring "0.11.0"]]

  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id           "dev"
                :source-paths ["src-cljs"]

                :figwheel     true

                :compiler     {:main                 client.core
                               :asset-path           "js/out"
                               :optimizations        :none
                               :output-dir           "resources/public/js/out"
                               :output-to            "resources/public/js/client.js"
                               :source-map-timestamp true
                               :source-map           true
                               :preloads             [devtools.preload]}}

               {:id           "min"
                :source-paths ["src-cljs"]
                :compiler     {:output-to     "resources/public/js/client.js"
                               :main          client.core
                               :optimizations :advanced
                               :pretty-print  false}}]}

  :figwheel {:css-dirs ["resources/public/css"]}

  :profiles {:dev {:dependencies  [[binaryage/devtools "0.9.9"]
                                   [figwheel-sidecar "0.5.16"]
                                   [cider/piggieback "0.3.1"]]
                   :source-paths  ["src-cljs" "dev" "src"]
                   :repl-options  {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]}
                   :clean-targets ^{:protect false} ["resources/public/js" :target-path]}})
