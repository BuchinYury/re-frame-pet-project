const socket = new WebSocket('ws://10.0.0.40:3030/ws');
socket.onopen = (event) => {
    console.log('Connection established...');
}
socket.onmessage = (event) => {
    console.log(event.data);
}
socket.onclose = (event) => {
    if (event.wasClean) {
        console.log('Connection closed. Clean exit.')
    } else {
        console.log(`Code: ${event.code}, Reason: ${event.reason}`);
    }
}
socket.onerror = (event) => {
    console.log(`Error: ${event.message}`);
    socket.close();
}

const sendMessage = (msg) => {
    console.log('Sending...');
    socket.send(msg);
}

const registerWaither = () => {
    let registerWaither = {
        method:	 "waiter/register",
        args:	{
            id: 1
        }
    }
    socket.send(JSON.stringify(registerWaither));
}